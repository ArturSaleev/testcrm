<p align="center">
<img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></p>


## About project

Test application CRM System

## Instalation
- $ composer create-project --prefer-dist laravel/laravel testCRM
- $ composer require laravel/ui
- $ composer require laravel/passport
- $ composer require laravel/socialite
- $ composer require ibex/crud-generator --dev
- $ composer require laravel-lang/lang:~7.0

- $ php artisan migrate
- $ php artisan passport:install
- $ php artisan vendor:publish --tag=crud
- $ php artisan passport:client --personal
- $ php artisan make:controller AuthController

## Source CRUD 
- https://github.com/awais-vteams/laravel-crud-generator

## Set Configuration Passport
File: app/Models/User.php 

    use Laravel\Passport\HasApiTokens;
    ...
    class User extends Authenticatable
    {
        use HasFactory, Notifiable, HasApiTokens;

File: app/Providers/AuthServiceProvider.php
    
    use Laravel\Passport\Passport;
    ...
    protected $policies = [
        'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];
    
    public function boot()
    {
        ...
        Passport::routes();        
    }
    
File: config/auth.php

    ...
    'api' => [
                'driver' => 'passport',
    ...    

## Start server
php artisan serve --host=laravel.home --port=80


## Fast start project
Download the project to a folder or directory

Create the "test_crm" database

Set the settings in the .env file

Run the command 
- $ composer require
- $ php artisan serve --host=laravel.home --port=80

## Screenshot
![Screenshot](./public/img/p1.png)

![Screenshot](./public/img/p2.png)
