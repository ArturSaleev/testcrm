<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Customer
 *
 * @property $id
 * @property $lastname
 * @property $firstname
 * @property $middlename
 * @property $birthday
 * @property $address
 * @property $email
 * @property $phone
 * @property $websait
 * @property $description
 *
 * @property Deal[] $deals
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Customer extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    static $rules = [
		'lastname' => 'required',
		'firstname' => 'required',
    ];

    protected $dates = ['birthday'];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['lastname','firstname','middlename','birthday','address','email','phone','websait','description'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deals()
    {
        return $this->hasMany('App\Deal', 'id_customers', 'id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->lastname} {$this->firstname} {$this->middlename}";
    }

}
