<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Deal
 *
 * @property $id
 * @property $name
 * @property $description
 * @property $amount
 * @property $id_stage
 * @property $id_customers
 * @property $id_user
 * @property $date_set
 *
 * @property Customer $customer
 * @property DirStage $dirStage
 * @property User $user
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Deal extends Model
{
    protected $primaryKey = 'id';

    static $rules = [
        'name' => 'required',
        'amount' => 'required',
        'id_stage' => 'required',
        'id_customers' => 'required',
        'id_user' => 'required',
        'date_set' => 'required'
    ];

    protected $dates = ['date_set'];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','description','amount','id_stage','id_customers','id_user', 'date_set'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function customer()
    {
        return $this->hasOne('App\Customer', 'id', 'id_customers');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function dirStage()
    {
        return $this->hasOne('App\DirStage', 'id', 'id_stage');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'id_user');
    }

}
