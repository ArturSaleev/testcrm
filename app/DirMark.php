<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DirMark
 *
 * @property $id
 * @property $name
 * @property $color
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class DirMark extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    static $rules = [
		'name' => 'required',
		'color' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','color'];



}
