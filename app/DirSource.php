<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DirSource
 *
 * @property $id
 * @property $name
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class DirSource extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    static $rules = [
		'name' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];



}
