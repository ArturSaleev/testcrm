<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DirStage
 *
 * @property $id
 * @property $name
 * @property $num_pp
 * @property $color
 *
 * @property Deal[] $deals
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class DirStage extends Model
{
    protected $primaryKey = 'id';
    public $timestamps = false;

    static $rules = [
		'name' => 'required',
		'num_pp' => 'required',
		'color' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name','num_pp','color'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function deals()
    {
        return $this->hasMany('App\Deal', 'id_stage', 'id');
    }


}
