<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Hash;

class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $token = $user->createToken(env('APP_KEY'))->accessToken;

        return $this->returnJson(true, '', ['token' => $token], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if( Auth::attempt($data) ) {
            $user = Auth::user();
            $token = $user->createToken($user->email.'-'.now());
            return $this->returnJson(true, '', ['token' => $token->accessToken]);
        } else {
            return $this->returnJson(false, 'Unauthorised', [], 401);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function profile()
    {
        try {
            $user = auth('api')->user();
        }catch(\Exception $e){
            return $this->returnJson(false, $e->getMessage(), []);;
        }

        if(!$user){
            return $this->returnJson(false, 'Unauthorised', [], 401);
        }

        return $this->returnJson(true, '', $user);;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        if(auth('api')->check()){
            auth('api')->user()->token()->revoke();
        }
        return $this->returnJson(true, '', []);
    }
}
