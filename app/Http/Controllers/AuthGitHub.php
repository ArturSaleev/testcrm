<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthGitHub extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }


    public function handleProviderCallback(Request $request)
    {
        $user = Socialite::driver('github')->user();

        $id = $user->getId();
        $name = ($user->getName() == '') ? $user->getNickname() : $user->getName();
        $email = $user->getEmail();
        $avatar = $user->getAvatar();

        $data = [
            'email' => $email,
            'social' => 'github',
            'social_id' => $id,
            'password' => $id
        ];

        $authAttempt = Auth::attempt($data);
        if(!$authAttempt){
            try {
                User::create([
                    'name' => $name,
                    'email' => $email,
                    'password' => bcrypt($id),
                    'social' => 'github',
                    'social_id' => $id,
                    'avatar' => $avatar
                ]);
            }catch (\Exception $e){
                return abort(401);
            }
            Auth::attempt($data);
        }
        return response()->redirectTo('/home');
    }
}
