<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function returnJson($success, $message, $result, $page = 200)
    {
        return response()->json(['success' => $success, 'message' => $message, 'result' => $result], $page);
    }

    public function selfView($view, $array, $page = null)
    {
        if($this->request) {
            try {
                $isAppJson = ($this->request->wantsJson()) ? true : false;
            } catch (\Exception $e) {
                $isAppJson = false;
            }
        }else{
            $isAppJson = false;
        }

        if($isAppJson){
            return $this->returnJson(true, '', $array);
        }else {
            if ($page !== null) {
                return view($view, $array)->with('i', $page);
            }

            return view($view, $array);
        }
    }
}
