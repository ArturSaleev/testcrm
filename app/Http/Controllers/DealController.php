<?php

namespace App\Http\Controllers;

use App\Deal;
use App\DirStage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Customer;
use Illuminate\Support\Facades\DB;

/**
 * Class DealController
 * @package App\Http\Controllers
 */
class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deals = Deal::paginate();

        return view('deal.index', compact('deals'))
            ->with('i', (request()->input('page', 1) - 1) * $deals->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $deal = new Deal();
        $customer = Customer::select(DB::raw("concat(lastname, ' ', firstname, ' ', middlename) as name"), 'id')->pluck("name", "id");
        $users = User::pluck('name', 'id');
        $dirstage = DirStage::orderBy('id')->pluck('name', 'id');
        return view('deal.create', ["deal" => $deal, 'customer' => $customer, 'users' => $users, 'dirstage' => $dirstage]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Deal::$rules);

        $deal = Deal::create($request->all());

        return redirect()->route('deal.index')
            ->with('success', 'Deal created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $deal = Deal::find($id);
        return view('deal.show', compact('deal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $deal = Deal::find($id);
        $customer = Customer::select(DB::raw("concat(lastname, ' ', firstname, ' ', middlename) as name"), 'id')->pluck("name", "id");
        $users = User::pluck('name', 'id');
        $dirstage = DirStage::orderBy('id')->pluck('name', 'id');

        return view('deal.edit', ["deal" => $deal, 'customer' => $customer, 'users' => $users, 'dirstage' => $dirstage]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Deal $deal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate(Deal::$rules);
        Deal::find($id)->update($request->all());

        return redirect()->route('deal.index')
            ->with('success', 'Deal updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        Deal::find($id)->delete();

        return redirect()->route('deal.index')
            ->with('success', 'Deal deleted successfully');
    }
}
