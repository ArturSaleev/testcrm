<?php

namespace App\Http\Controllers;

use App\DirMark;
use Illuminate\Http\Request;

/**
 * Class DirMarkController
 * @package App\Http\Controllers
 */
class DirMarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dirMarks = DirMark::paginate();
        return $this->selfView('dir-mark.index', compact('dirMarks'), (request()->input('page', 1) - 1) * $dirMarks->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dirMark = new DirMark();
        return view('dir-mark.create', compact('dirMark'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(DirMark::$rules);

        $dirMark = DirMark::create($request->all());

        return redirect()->route('dirmark.index')
            ->with('success', 'Guide Mark created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dirMark = DirMark::find($id);

        return $this->selfView('dir-mark.show', compact('dirMark'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dirMark = DirMark::find($id);

        return $this->selfView('dir-mark.edit', compact('dirMark'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  DirMark $dirMark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate(DirMark::$rules);
        DirMark::find($id)->update($request->all());
        return redirect()->route('dirmark.index')
            ->with('success', 'Guide Mark updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        DirMark::find($id)->delete();
        return redirect()->route('dirmark.index')
            ->with('success', 'Guide Mark deleted successfully');
    }
}
