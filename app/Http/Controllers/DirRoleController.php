<?php

namespace App\Http\Controllers;

use App\DirRole;
use Illuminate\Http\Request;

/**
 * Class DirRoleController
 * @package App\Http\Controllers
 */
class DirRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dirRoles = DirRole::paginate();
        return $this->selfView('dir-role.index', compact('dirRoles'), (request()->input('page', 1) - 1) * $dirRoles->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dirRole = new DirRole();
        return view('dir-role.create', compact('dirRole'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(DirRole::$rules);

        $dirRole = DirRole::create($request->all());

        return redirect()->route('dirrole.index')
            ->with('success', 'DirRole created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dirRole = DirRole::find($id);

        return view('dir-role.show', compact('dirRole'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dirRole = DirRole::find($id);

        return view('dir-role.edit', compact('dirRole'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  DirRole $dirRole
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate(DirRole::$rules);
        DirRole::find($id)->update($request->all());

        return redirect()->route('dirrole.index')
            ->with('success', 'DirRole updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $dirRole = DirRole::find($id)->delete();

        return redirect()->route('dirrole.index')
            ->with('success', 'DirRole deleted successfully');
    }
}
