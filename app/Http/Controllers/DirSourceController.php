<?php

namespace App\Http\Controllers;

use App\DirSource;
use Illuminate\Http\Request;

/**
 * Class DirSourceController
 * @package App\Http\Controllers
 */
class DirSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dirSources = DirSource::paginate();

        return view('dir-source.index', compact('dirSources'))
            ->with('i', (request()->input('page', 1) - 1) * $dirSources->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dirSource = new DirSource();
        return view('dir-source.create', compact('dirSource'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(DirSource::$rules);

        $dirSource = DirSource::create($request->all());

        return redirect()->route('dirsource.index')
            ->with('success', 'DirSource created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dirSource = DirSource::find($id);

        return view('dir-source.show', compact('dirSource'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dirSource = DirSource::find($id);

        return view('dir-source.edit', compact('dirSource'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  DirSource $dirSource
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate(DirSource::$rules);
        DirSource::find($id)->update($request->all());

        return redirect()->route('dirsource.index')
            ->with('success', 'DirSource updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $dirSource = DirSource::find($id)->delete();

        return redirect()->route('dirsource.index')
            ->with('success', 'DirSource deleted successfully');
    }
}
