<?php

namespace App\Http\Controllers;

use App\DirStage;
use Illuminate\Http\Request;

/**
 * Class DirStageController
 * @package App\Http\Controllers
 */
class DirStageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dirStages = DirStage::paginate();

        return view('dir-stage.index', compact('dirStages'))
            ->with('i', (request()->input('page', 1) - 1) * $dirStages->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dirStage = new DirStage();
        return view('dir-stage.create', compact('dirStage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(DirStage::$rules);

        $dirStage = DirStage::create($request->all());

        return redirect()->route('dirstage.index')
            ->with('success', 'DirStage created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dirStage = DirStage::find($id);

        return view('dir-stage.show', compact('dirStage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dirStage = DirStage::find($id);

        return view('dir-stage.edit', compact('dirStage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  DirStage $dirStage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate(DirStage::$rules);
        DirStage::find($id)->update($request->all());

        return redirect()->route('dirstage.index')
            ->with('success', 'DirStage updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        DirStage::find($id)->delete();

        return redirect()->route('dirstage.index')
            ->with('success', 'DirStage deleted successfully');
    }
}
