<?php

namespace App\Http\Controllers;

use App\Deal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $deal_stage = Deal::select('id_stage', DB::raw('count(*) as cnt, sum(amount) as amount'))
            ->where('date_set', '>=', date('Y-m-d'))
            ->groupBy('id_stage')->get();

        $deal_money_now = Deal::select(DB::raw("SUM(amount) AS amount, DATE_FORMAT(date_set, '%m') AS dateset"))
            ->where(DB::raw("DATE_FORMAT(date_set, '%Y')"), '>=', date('Y'))
            ->groupBy(DB::raw("DATE_FORMAT(date_set, '%m')"))
            ->orderBy('date_set')
            ->get();
        $deal_money = [];
        foreach($deal_money_now as $dm)
        {
            $deal_money[$dm->dateset] = $dm->amount;
        }

        return $this->selfView('home', ['deal_stage' => $deal_stage, 'deal_money' => $deal_money]);
    }
}
