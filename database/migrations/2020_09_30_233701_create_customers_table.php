<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table)
		{
			$table->id();
			$table->string('lastname')->comment('Фамилия');
			$table->string('firstname')->comment('Имя');
			$table->string('middlename')->nullable()->comment('Отчество');
			$table->date('birthday')->nullable()->comment('Дата рождения');
			$table->text('address')->nullable()->comment('Адрес');
			$table->string('email')->nullable();
			$table->string('phone')->nullable();
			$table->string('websait')->nullable();
			$table->text('description')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}

}
