<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDealsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('deals', function(Blueprint $table)
		{
			$table->id();
			$table->string('name')->comment('Название');
			$table->text('description')->nullable()->comment('Описание');
			$table->decimal(''amount'', 20)->default(0.00)->comment('Сумма сделки');
			$table->bigInteger('id_stage')->default(0)->index('FK_deal_dir_stages_id');
			$table->bigInteger('id_customers')->default(0)->index('FK_deal_customers_id')->comment('Клиент');
			$table->bigInteger('id_user')->unsigned()->default(0)->index('FK_deal_users_id');
			$table->dateTime('date_set')->nullable();
			$table->timestamps(10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('deals');
	}

}
