<div class="box box-info padding-1">
    <div class="box-body">
        <div class="row">
            <div class="form-group col-lg-3">
                {{ Form::label(__('validation.attributes.last_name')) }}
                {{ Form::text('lastname', $customer->lastname, ['class' => 'form-control' . ($errors->has('lastname') ? ' is-invalid' : ''), 'placeholder' => __('validation.attributes.last_name')]) }}
                {!! $errors->first('lastname', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group col-lg-3">
                {{ Form::label(__('validation.attributes.first_name')) }}
                {{ Form::text('firstname', $customer->firstname, ['class' => 'form-control' . ($errors->has('firstname') ? ' is-invalid' : ''), 'placeholder' => __('validation.attributes.first_name')]) }}
                {!! $errors->first('firstname', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group col-lg-3">
                {{ Form::label(__('validation.attributes.middle_name')) }}
                {{ Form::text('middlename', $customer->middlename, ['class' => 'form-control' . ($errors->has('middlename') ? ' is-invalid' : ''), 'placeholder' => __('validation.attributes.middle_name')]) }}
                {!! $errors->first('middlename', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group col-lg-3">
                {{ Form::label(__('validation.attributes.birthday')) }}
                {{ Form::date('birthday', $customer->birthday, ['class' => 'form-control' . ($errors->has('birthday') ? ' is-invalid' : ''), 'placeholder' => __('validation.attributes.birthday')]) }}
                {!! $errors->first('birthday', '<div class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-lg-4">
                {{ Form::label(__('validation.attributes.email')) }}
                {{ Form::text('email', $customer->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => __('validation.attributes.email')]) }}
                {!! $errors->first('email', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group col-lg-4">
                {{ Form::label(__('validation.attributes.phone')) }}
                {{ Form::text('phone', $customer->phone, ['class' => 'form-control' . ($errors->has('phone') ? ' is-invalid' : ''), 'placeholder' => __('validation.attributes.phone')]) }}
                {!! $errors->first('phone', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group col-lg-4">
                {{ Form::label(__('validation.attributes.websait')) }}
                {{ Form::text('websait', $customer->websait, ['class' => 'form-control' . ($errors->has('websait') ? ' is-invalid' : ''), 'placeholder' => __('validation.attributes.websait')]) }}
                {!! $errors->first('websait', '<div class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-lg-6">
                {{ Form::label(__('validation.attributes.address')) }}
                {{ Form::textarea('address', $customer->address, ['class' => 'form-control' . ($errors->has('address') ? ' is-invalid' : ''), 'placeholder' => __('validation.attributes.address')]) }}
                {!! $errors->first('address', '<div class="invalid-feedback">:message</p>') !!}
            </div>

            <div class="form-group col-lg-6">
                {{ Form::label(__('all.table.Description')) }}
                {{ Form::textarea('description', $customer->description, ['class' => 'form-control' . ($errors->has('description') ? ' is-invalid' : ''), 'placeholder' => __('all.table.Description')]) }}
                {!! $errors->first('description', '<div class="invalid-feedback">:message</p>') !!}
            </div>
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('all.button.Submit') }}</button>
    </div>
</div>
