@extends('layouts.app')

@section('template_title')
    {{ __('all.Customers') }}
@endsection

@section('template_button')
    <a href="{{ route('customer.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
       <i class="fas fa-plus-circle"></i> {{ __('all.button.CreateNew') }}
    </a>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        @include('layouts.alerts.success')

                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>{{ __('all.table.No') }}</th>

										<th>{{ __('validation.attributes.last_name') }}</th>
										<th>{{ __('validation.attributes.first_name') }}</th>
										<th>{{ __('validation.attributes.middle_name') }}</th>
										<th>{{ __('validation.attributes.birthday') }}</th>
										<th>{{ __('validation.attributes.address') }}</th>
										<th>{{ __('validation.attributes.email') }}</th>
										<th>{{ __('validation.attributes.phone') }}</th>
										<th>{{ __('validation.attributes.websait') }}</th>
										<th>{{ __('all.table.Description') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($customers as $customer)
                                        <tr>
                                            <td>{{ ++$i }}</td>

											<td>{{ $customer->lastname }}</td>
											<td>{{ $customer->firstname }}</td>
											<td>{{ $customer->middlename }}</td>
											<td>{{ $customer->birthday->format('d.m.Y') }}</td>
											<td>{{ $customer->address }}</td>
											<td>{{ $customer->email }}</td>
											<td>{{ $customer->phone }}</td>
											<td>{{ $customer->websait }}</td>
											<td>{{ $customer->description }}</td>

                                            <td>
                                                <form action="{{ route('customer.destroy',$customer->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('customer.show',$customer->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('all.button.Show') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('customer.edit',$customer->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('all.button.Edit') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('all.button.Delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $customers->links() !!}
            </div>
        </div>
    </div>
@endsection
