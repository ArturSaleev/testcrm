@extends('layouts.app')

@section('template_title')
    {{ __('all.button.Show') }} {{ __('all.Customers') }}
@endsection

@section('template_button')
    <a href="{{ route('customer.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> {{ __('all.button.Back') }}</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">

                        <div class="form-group">
                            <strong>{{ __('validation.attributes.last_name') }}:</strong>
                            {{ $customer->lastname }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('validation.attributes.first_name') }}:</strong>
                            {{ $customer->firstname }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('validation.attributes.middle_name') }}:</strong>
                            {{ $customer->middlename }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('validation.attributes.birthday') }}:</strong>
                            {{ $customer->birthday->format('d.m.Y') }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('validation.attributes.address') }}:</strong>
                            {{ $customer->address }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('validation.attributes.email') }}:</strong>
                            {{ $customer->email }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('validation.attributes.phone') }}:</strong>
                            {{ $customer->phone }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('validation.attributes.websait') }}:</strong>
                            {{ $customer->websait }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.Description') }}:</strong>
                            {{ $customer->description }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
