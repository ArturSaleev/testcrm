@extends('layouts.app')

@section('template_title')
    {{ __('all.Update') }} {{ __('all.Deal') }}
@endsection

@section('template_button')
    <a href="{{ route('deal.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> {{ __('all.button.Back') }}</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">
                <div class="card card-default">
                    @includeif('partials.errors')
                    <div class="card-body">
                        <form method="POST" action="{{ route('deal.update', $deal->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('deal.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
