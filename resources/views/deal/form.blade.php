
<div class="box box-info padding-1">
    <div class="box-body row">
        <div class="col-lg-8">
            <div class="form-group">
                {{ Form::label(__('all.table.Name')) }}
                {{ Form::text('name', $deal->name, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => __('all.table.Name')]) }}
                {!! $errors->first('name', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group">
                {{ Form::label(__('all.table.Description')) }}
                {{ Form::textarea('description', $deal->description, ['class' => 'form-control' . ($errors->has('description') ? ' is-invalid' : ''), 'placeholder' => __('all.table.Description')]) }}
                {!! $errors->first('description', '<div class="invalid-feedback">:message</p>') !!}
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                {{ Form::label(__('all.table.amount')) }}
                {{ Form::number('amount', $deal->amount, ['class' => 'form-control' . ($errors->has('amount') ? ' is-invalid' : ''), 'placeholder' => __('all.table.Amount')]) }}
                {!! $errors->first('amount', '<div class="invalid-feedback">:message</p>') !!}
            </div>

            <div class="form-group">
                {{ Form::label(__('all.table.id_stage')) }}
                {{ Form::select('id_stage', $dirstage, ($deal->id_stage ?? 1), ['class' => 'form-control' . ($errors->has('id_stage') ? ' is-invalid' : '')]) }}
                {!! $errors->first('id_stage', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group">
                {{ Form::label(__('all.table.id_customers')) }}
                {{ Form::select('id_customers', $customer, ($deal->id_customers ?? 1), ['class' => 'form-control' . ($errors->has('id_stage') ? ' is-invalid' : '')]) }}
                {!! $errors->first('id_customers', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group">
                {{ Form::label(__('all.table.id_user')) }}
                {{ Form::select('id_user', $users, ($deal->id_user ?? Auth::id()), ['class' => 'form-control' . ($errors->has('id_stage') ? ' is-invalid' : '')]) }}
                {!! $errors->first('id_user', '<div class="invalid-feedback">:message</p>') !!}
            </div>
            <div class="form-group">
                {{ Form::label(__('all.table.date_set')) }}
                {{ Form::date('date_set', ($deal->date_set ?? date('Y-m-d')), ['class' => 'form-control' . ($errors->has('date_set') ? ' is-invalid' : ''), 'placeholder' => __('all.table.date_set')]) }}
                {!! $errors->first('date_set', '<div class="invalid-feedback">:message</p>') !!}
            </div>
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('all.button.Submit') }}</button>
    </div>
</div>
