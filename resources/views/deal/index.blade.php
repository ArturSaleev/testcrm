@extends('layouts.app')

@section('template_title')
    {{ __('all.Deal') }}
@endsection

@section('template_button')
    <a href="{{ route('deal.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
        <i class="fas fa-plus-circle"></i> {{ __('all.button.CreateNew') }}
    </a>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        @include('layouts.alerts.success')
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>{{ __('all.table.No') }}</th>

                                        <th>{{ __('all.table.Name') }}</th>
										<th>{{ __('all.table.Description') }}</th>
										<th>{{ __('all.table.Amount') }}</th>
										<th>{{ __('all.table.IdStage') }}</th>
										<th>{{ __('all.table.IdCustomers') }}</th>
										<th>{{ __('all.table.IdUser') }}</th>
                                        <th>{{ __('all.table.date_set') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($deals as $deal)
                                        <tr>
                                            <td>{{ ++$i }}</td>

											<td>{{ $deal->name }}</td>
											<td>{{ $deal->description }}</td>
											<td>{{ $deal->amount }}</td>
											<td>{{ $deal->dirStage->name }}</td>
											<td>{{ $deal->customer->getFullNameAttribute() }}</td>
											<td>{{ $deal->user->name }}</td>
                                            <td>{{ $deal->date_set->format('d.m.Y') }}</td>

                                            <td>
                                                <form action="{{ route('deal.destroy',$deal->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('deal.show',$deal->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('all.button.Show') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('deal.edit',$deal->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('all.button.Edit') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('all.button.Delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $deals->links() !!}
            </div>
        </div>
    </div>
@endsection
