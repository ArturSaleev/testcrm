@extends('layouts.app')

@section('template_title')
    {{ __('all.button.Show') }} {{ __('all.Deal') }}
@endsection

@section('template_button')
    <a href="{{ route('deal.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> {{ __('all.button.Back') }}</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">

                        <div class="form-group">
                            <strong>{{ __('all.table.Name') }}:</strong>
                            {{ $deal->name }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.Description') }}:</strong>
                            {{ $deal->description }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.Amount') }}:</strong>
                            {{ $deal->amount }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.IdStage') }}:</strong>
                            {{ $deal->dirStage->name }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.IdCustomers') }}:</strong>
                            {{ $deal->customer->getFullNameAttribute() }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.IdUser') }}:</strong>
                            {{ $deal->user->name }}
                        </div>

                        <div class="form-group">
                            <strong>{{ __('all.table.date_set') }}:</strong>
                            {{ $deal->date_set->format('d.m.Y') }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
