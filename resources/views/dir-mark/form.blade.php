<div class="box box-info padding-1">
    <div class="box-body">

        <div class="form-group">
            {{ Form::label( __('all.table.Name') ) }}
            {{ Form::text('name', $dirMark->name, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => __('all.table.Name') ]) }}
            {!! $errors->first('name', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label(__('all.table.Color')) }}
            {{ Form::color('color', $dirMark->color, ['class' => 'form-control' . ($errors->has('color') ? ' is-invalid' : ''), 'placeholder' => __('all.table.Color')]) }}
            {!! $errors->first('color', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('all.button.Submit') }}</button>
    </div>
</div>
