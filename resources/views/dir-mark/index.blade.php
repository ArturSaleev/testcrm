@extends('layouts.app')

@section('template_title')
    {{ __('all.GuidesMarks') }}
@endsection

@section('template_button')
    <a href="{{ route('dirmark.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
        <i class="fas fa-plus-circle"></i> {{ __('all.button.CreateNew') }}
    </a>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        @include('layouts.alerts.success')

                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>{{ __('all.table.No') }}</th>

										<th>{{ __('all.table.Name') }}</th>
										<th>{{ __('all.table.Color') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dirMarks as $dirMark)
                                        <tr>
                                            <td>{{ ++$i }}</td>

											<td>{{ $dirMark->name }}</td>
											<td>{{ $dirMark->color }} <span class="badge" style="background-color: {{ $dirMark->color }}; color: #ffffff">Color View</span></td>

                                            <td>
                                                <form action="{{ route('dirmark.destroy',$dirMark->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('dirmark.show',$dirMark->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('all.button.Show') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('dirmark.edit',$dirMark->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('all.button.Edit') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('all.button.Delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $dirMarks->links() !!}
            </div>
        </div>
    </div>
@endsection
