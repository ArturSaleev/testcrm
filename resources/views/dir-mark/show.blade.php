@extends('layouts.app')

@section('template_title')
    {{ __('all.button.Show') }} {{ __('all.GuideMark') }}
@endsection

@section('template_button')
    <a href="{{ route('dirmark.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> Back</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <div class="form-group">
                            <strong>{{ __('all.table.Name') }}:</strong>
                            {{ $dirMark->name }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.Color') }}:</strong>
                            {{ $dirMark->color }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
