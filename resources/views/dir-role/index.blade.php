@extends('layouts.app')

@section('template_title')
    {{ __('all.DirRole') }}
@endsection

@section('template_button')
    <a href="{{ route('dirrole.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
        <i class="fas fa-plus-circle"></i> {{ __('all.button.CreateNew') }}
    </a>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @include('layouts.alerts.success')

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>{{ __('all.table.No') }}</th>

										<th>{{ __('all.table.Name') }}</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dirRoles as $dirRole)
                                        <tr>
                                            <td>{{ ++$i }}</td>

											<td>{{ $dirRole->name }}</td>

                                            <td>
                                                <form action="{{ route('dirrole.destroy',$dirRole->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('dirrole.show',$dirRole->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('all.button.Show') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('dirrole.edit',$dirRole->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('all.button.Edit') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('all.button.Delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $dirRoles->links() !!}
            </div>
        </div>
    </div>
@endsection
