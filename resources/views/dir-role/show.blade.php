@extends('layouts.app')

@section('template_title')
    {{ __('all.button.Show') }} {{ __('all.DirRole') }}
@endsection

@section('template_button')
    <a href="{{ route('dirrole.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> {{ __('all.button.Back') }}</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <div class="form-group">
                            <strong>{{ __('all.table.Name') }}:</strong>
                            {{ $dirRole->name }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
