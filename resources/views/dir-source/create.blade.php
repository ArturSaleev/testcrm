@extends('layouts.app')

@section('template_title')
    {{ __('all.Create') }} {{ __('all.DirSource') }}
@endsection

@section('template_button')
    <a href="{{ route('dirsource.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> {{ __('all.button.Back') }}</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-body">
                        <form method="POST" action="{{ route('dirsource.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('dir-source.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
