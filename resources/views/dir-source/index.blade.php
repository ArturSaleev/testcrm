@extends('layouts.app')

@section('template_title')
    {{ __('all.DirSource') }}
@endsection

@section('template_button')
    <a href="{{ route('dirsource.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
        <i class="fas fa-plus-circle"></i> {{ __('all.button.CreateNew') }}
    </a>
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    @include('layouts.alerts.success')

                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead class="thead">
                                <tr>
                                    <th>{{ __('all.table.No') }}</th>

                                    <th>{{ __('all.table.Name') }}</th>

                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($dirSources as $dirSource)
                                    <tr>
                                        <td>{{ ++$i }}</td>

                                        <td>{{ $dirSource->name }}</td>

                                        <td>
                                            <form action="{{ route('dirsource.destroy',$dirSource->id) }}" method="POST">
                                                <a class="btn btn-sm btn-primary " href="{{ route('dirsource.show',$dirSource->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('all.button.Show') }}</a>
                                                <a class="btn btn-sm btn-success" href="{{ route('dirsource.edit',$dirSource->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('all.button.Edit') }}</a>
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('all.button.Delete') }}</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    {!! $dirSources->links() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
