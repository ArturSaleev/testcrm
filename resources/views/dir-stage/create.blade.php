@extends('layouts.app')

@section('template_title')
    {{ __('all.Create') }} {{ __('all.DirStage') }}
@endsection

@section('template_button')
    <a href="{{ route('dirstage.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> {{ __('all.button.Back') }}</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-body">
                        <form method="POST" action="{{ route('dirstage.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('dir-stage.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
