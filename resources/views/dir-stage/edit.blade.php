@extends('layouts.app')

@section('template_title')
    {{ __('all.Update') }} {{ __('all.DirStage') }}
@endsection

@section('template_button')
    <a href="{{ route('dirstage.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> {{ __('all.button.Back') }}</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-body">
                        <form method="POST" action="{{ route('dirstage.update', $dirStage->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('dir-stage.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
