<div class="box box-info padding-1">
    <div class="box-body">

        <div class="form-group">
            {{ Form::label(__('all.table.Name')) }}
            {{ Form::text('name', $dirStage->name, ['class' => 'form-control' . ($errors->has('name') ? ' is-invalid' : ''), 'placeholder' => __('all.table.Name')]) }}
            {!! $errors->first('name', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label(__('all.table.num_pp')) }}
            {{ Form::text('num_pp', $dirStage->num_pp, ['class' => 'form-control' . ($errors->has('num_pp') ? ' is-invalid' : ''), 'placeholder' => __('all.table.num_pp')]) }}
            {!! $errors->first('num_pp', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label(__('all.table.Color')) }}
            {{ Form::color('color', $dirStage->color, ['class' => 'form-control' . ($errors->has('color') ? ' is-invalid' : ''), 'placeholder' => __('all.table.Color')]) }}
            {!! $errors->first('color', '<div class="invalid-feedback">:message</p>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('all.button.Submit') }}</button>
    </div>
</div>
