@extends('layouts.app')

@section('template_title')
    {{ __('all.button.Show') }} {{ __('all.DirStage') }}
@endsection

@section('template_button')
    <a href="{{ route('dirstage.index') }}" class="btn btn-primary btn-sm float-right"  data-placement="left"> {{ __('all.button.Back') }}</a>
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <div class="form-group">
                            <strong>{{ __('all.table.Name') }}:</strong>
                            {{ $dirStage->name }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.num_pp') }}:</strong>
                            {{ $dirStage->num_pp }}
                        </div>
                        <div class="form-group">
                            <strong>{{ __('all.table.Color') }}:</strong>
                            {{ $dirStage->color }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
