@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{ __('all.form.Main') }}</h1>
    </div>

    <div class="row">
        @foreach($deal_stage as $ds)
        <div class="col-xl-2 col-md-4 mb-4">
            <div class="card shadow h-100 py-2" style="border-left:.25rem solid {{ $ds->dirStage->color }}!important">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">{{ $ds->dirStage->name }}</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $ds->amount }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('all.label.deal_now_year') }}</div>

                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="deal_now_year"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('vendor/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script>
        SetChatLine('deal_now_year', JSON.parse('{!! json_encode($deal_money) !!}'), JSON.parse('{!! json_encode(\Illuminate\Support\Facades\Lang::get('all.month')) !!}'));
    </script>
@endsection
