<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ asset('/') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-cat"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Test <sup>CRM</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ asset('/') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>{{ __('all.button.Main') }}</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        {{ __('all.label.Mainmenu') }}
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>{{ __('all.Guides') }}</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ asset('dirmark') }}"><i class="fas fa-highlighter"></i> {{ __('all.Marks') }}</a>
                <a class="collapse-item" href="{{ asset('dirrole') }}"><i class="fas fa-user-secret"></i> {{ __('all.Roles') }}</a>
                <a class="collapse-item" href="{{ asset('dirsource') }}"><i class="fas fa-laptop"></i> {{ __('all.DataSource') }}</a>
                <a class="collapse-item" href="{{ asset('dirstage') }}"><i class="fas fa-shoe-prints"></i> {{ __('all.Stages') }}</a>
            </div>
        </div>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ asset('customer') }}">
            <i class="fas fa-fw fa-users"></i>
            <span>{{ __('all.Customers') }}</span></a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ asset('deal') }}">
            <i class="fas fa-fw fa-chart-line"></i>
            <span>{{ __('all.Deals') }}</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
