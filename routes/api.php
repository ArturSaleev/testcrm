<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DirMarkController;
use App\Http\Controllers\DealController;
use App\Http\Controllers\DirRoleController;
use App\Http\Controllers\DirSourceController;
use App\Http\Controllers\DirStageController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::get('/profile', [AuthController::class, 'profile']);
Route::get('/logout', [AuthController::class, 'logout']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('dirmark', DirMarkController::class);
Route::resource('dirrole', DirRoleController::class);
Route::resource('dirsource', DirSourceController::class);
Route::resource('dirstage', DirStageController::class);
Route::resource('customer', CustomerController::class);
Route::resource('deal', DealController::class);
Route::resource('user', UserController::class);

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
