<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthGitHub;
use App\Http\Controllers\DirMarkController;
use App\Http\Controllers\DealController;
use App\Http\Controllers\DirRoleController;
use App\Http\Controllers\DirSourceController;
use App\Http\Controllers\DirStageController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('locale/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'ru', 'de'])) {
        abort(400);
    }
    Session::put('locale', $locale);
    return redirect()->back();
})->name('locale');

Route::get('/', function () {
    return view('welcome');
});

Route::get('login/github', [AuthGitHub::class, 'redirectToProvider']);
Route::get('login/github/callback', [AuthGitHub::class, 'handleProviderCallback']);

Auth::routes();
Route::group( ['middleware' => 'auth' ], function() {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::resource('dirmark', DirMarkController::class);
    Route::resource('dirrole', DirRoleController::class);
    Route::resource('dirsource', DirSourceController::class);
    Route::resource('dirstage', DirStageController::class);
    Route::resource('customer', CustomerController::class);
    Route::resource('deal', DealController::class);
    Route::resource('user', UserController::class);
});
