-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Окт 01 2020 г., 05:38
-- Версия сервера: 10.4.14-MariaDB
-- Версия PHP: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test_crm`
--

-- --------------------------------------------------------

--
-- Структура таблицы `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) NOT NULL,
  `lastname` varchar(255) NOT NULL COMMENT 'Фамилия',
  `firstname` varchar(255) NOT NULL COMMENT 'Имя',
  `middlename` varchar(255) DEFAULT NULL COMMENT 'Отчество',
  `birthday` date DEFAULT NULL COMMENT 'Дата рождения',
  `address` text DEFAULT NULL COMMENT 'Адрес',
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `websait` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Контакты, клиенты';

--
-- Дамп данных таблицы `customers`
--

INSERT INTO `customers` (`id`, `lastname`, `firstname`, `middlename`, `birthday`, `address`, `email`, `phone`, `websait`, `description`) VALUES
(1, 'Тестов', 'Тест', 'Тестович', '1990-01-01', 'Адрес проживания', 'test@mail.ru', '8123123123123', 'http://test.net', 'Маленькое описание'),
(2, 'Jonni', 'Dep', NULL, '1980-01-01', 'No address', 'Jonni@mail.com', '8123456789', 'Jonni.com', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `deals`
--

CREATE TABLE `deals` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `description` text DEFAULT NULL COMMENT 'Описание',
  `amount` decimal(20,2) NOT NULL DEFAULT 0.00 COMMENT 'Сумма сделки',
  `id_stage` bigint(20) NOT NULL DEFAULT 0,
  `id_customers` bigint(20) NOT NULL DEFAULT 0 COMMENT 'Клиент',
  `id_user` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `date_set` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Сделки';

--
-- Дамп данных таблицы `deals`
--

INSERT INTO `deals` (`id`, `name`, `description`, `amount`, `id_stage`, `id_customers`, `id_user`, `date_set`, `created_at`, `updated_at`) VALUES
(1, 'Продажа сотовых телефонов Nokia', 'Продажа сотовых телефонов Nokia', '1000000.00', 1, 1, 10, '2020-10-01 00:00:00', '2020-12-01 17:12:17', '2020-09-30 17:18:04'),
(2, 'Запчасти', NULL, '500000.00', 3, 1, 11, '2020-11-02 00:00:00', '2020-10-15 17:44:27', '2020-09-30 17:44:27'),
(3, 'Мини сделка', NULL, '500.00', 1, 1, 11, '2020-10-05 00:00:00', '2020-09-30 18:24:38', '2020-09-30 18:24:38');

-- --------------------------------------------------------

--
-- Структура таблицы `dir_marks`
--

CREATE TABLE `dir_marks` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `color` varchar(50) NOT NULL COMMENT 'Цвет метки'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Справочник меток';

--
-- Дамп данных таблицы `dir_marks`
--

INSERT INTO `dir_marks` (`id`, `name`, `color`) VALUES
(6, 'принимает решение', '#ffa200'),
(7, 'договор согласован', '#00ffee'),
(8, 'ожидается оплата', '#40b017'),
(9, 'просрочено', '#000000'),
(10, 'дожим на покупку', '#fbff00'),
(11, 'требуется консультация', '#a3a3a3'),
(12, 'срочный заказ', '#ff0000');

-- --------------------------------------------------------

--
-- Структура таблицы `dir_roles`
--

CREATE TABLE `dir_roles` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Справочник ролей';

--
-- Дамп данных таблицы `dir_roles`
--

INSERT INTO `dir_roles` (`id`, `name`) VALUES
(5, 'Директор'),
(7, 'Клиент'),
(6, 'Менеджер');

-- --------------------------------------------------------

--
-- Структура таблицы `dir_sources`
--

CREATE TABLE `dir_sources` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Справочник источника получения данных';

--
-- Дамп данных таблицы `dir_sources`
--

INSERT INTO `dir_sources` (`id`, `name`) VALUES
(2, 'База данных'),
(3, 'Другие источники'),
(1, 'Интернет');

-- --------------------------------------------------------

--
-- Структура таблицы `dir_stages`
--

CREATE TABLE `dir_stages` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `num_pp` int(11) NOT NULL DEFAULT 0,
  `color` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Справочник этапов действий';

--
-- Дамп данных таблицы `dir_stages`
--

INSERT INTO `dir_stages` (`id`, `name`, `num_pp`, `color`) VALUES
(1, 'Нет результата', 1, '#878787'),
(2, 'Переговоры', 2, '#429eff'),
(3, 'Ожидаем оплату', 3, '#01f411'),
(4, 'Заключили договор', 4, '#ffc800');

-- --------------------------------------------------------

--
-- Структура таблицы `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_09_30_233511_create_dir_marks_table', 0),
(10, '2020_09_30_233626_create_dir_roles_table', 0),
(11, '2020_09_30_233638_create_dir_sources_table', 0),
(12, '2020_09_30_233648_create_dir_stages_table', 0),
(13, '2020_09_30_233701_create_customers_table', 0),
(14, '2020_09_30_233710_create_deals_table', 0),
(15, '2020_09_30_233711_add_foreign_keys_to_deals_table', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('39d031e1dcec964116e06d43163f0e2068b11be4c8f134592af86ea1121c2d23f2b001fe2e9be10e', 1, 1, 'Laravel Password Grant Client', '[]', 0, '2020-09-29 09:37:08', '2020-09-29 09:37:08', '2021-09-29 15:37:08'),
('507fc19df228c34a769cfddb3c6ea9126e5423a06c5a61d5111d1f17e9cbb4b088ae192daa0d3101', 1, 1, 'admin@mail.ru-2020-09-29 15:39:47', '[]', 0, '2020-09-29 09:39:48', '2020-09-29 09:39:48', '2021-09-29 15:39:48'),
('aa26a5c74486b9c597616ca6435773386c5ea894091095d6f46f9c32929e8d273a493cc230616080', 1, 1, 'admin@mail.ru-2020-09-29 15:40:21', '[]', 1, '2020-09-29 09:40:21', '2020-09-29 09:40:21', '2021-09-29 15:40:21'),
('bd6af67fa2711a0c25c81be94e72c4c59c80c12178ca79f2eb66eb1eb97cc622300231b35c28d423', 1, 1, 'admin@mail.ru-2020-09-29 16:09:08', '[]', 0, '2020-09-29 10:09:08', '2020-09-29 10:09:08', '2021-09-29 16:09:08'),
('eb65b89aff24e339d9ac300f4fc25a782a360155af46335ac481d6ac3b6283bbb79d26ce25f00296', 1, 1, 'admin@mail.ru-2020-09-29 16:26:54', '[]', 0, '2020-09-29 10:26:54', '2020-09-29 10:26:54', '2021-09-29 16:26:54');

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'TestCRM Personal Access Client', '7o9wbUcFx31RSY5rBvsfsePuGatk0bhnop8WaG6y', NULL, 'http://localhost', 1, 0, 0, '2020-09-29 09:35:19', '2020-09-29 09:35:19');

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-09-29 09:35:19', '2020-09-29 09:35:19');

-- --------------------------------------------------------

--
-- Структура таблицы `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isadmin` int(11) NOT NULL DEFAULT 0,
  `social` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `avatar`, `isadmin`, `social`, `social_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@mail.ru', NULL, '$2y$10$GAB2h/IwH11BbMDQd9A8o.VUgemkRFfWfvzBJNJyq5BFMlb7S.BZK', NULL, 1, NULL, NULL, 'HUK8o7RSiHIWR7CUzPEhbMgD31QI3OOhFOQMzYgUSzEkxbE8je6izNxjgcZl', NULL, NULL),
(10, 'Saleev', 'adlcom83@gmail.com', NULL, '$2y$10$jjU/BW87QktOLx0hAaPFNeqQfepGc6PtVqXMUZ25wfk1K33RJXCPu', 'https://avatars3.githubusercontent.com/u/15760830?v=4', 0, 'github', 15760830, NULL, '2020-09-29 02:07:13', '2020-09-29 02:07:13'),
(11, 'test name', 'test@mail.ru', NULL, '$2y$10$atSMy9JBR22FXId8oYnVM.Z6DFe2qttkh6R2F/R7M0pTdoApBQEsi', NULL, 0, NULL, NULL, NULL, '2020-09-29 06:11:25', '2020-09-29 06:11:25');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `deals`
--
ALTER TABLE `deals`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_deal_dir_stages_id` (`id_stage`),
  ADD KEY `FK_deal_customers_id` (`id_customers`),
  ADD KEY `FK_deal_users_id` (`id_user`);

--
-- Индексы таблицы `dir_marks`
--
ALTER TABLE `dir_marks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `dir_roles`
--
ALTER TABLE `dir_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `dir_sources`
--
ALTER TABLE `dir_sources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `dir_stages`
--
ALTER TABLE `dir_stages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Индексы таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Индексы таблицы `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Индексы таблицы `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Индексы таблицы `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `deals`
--
ALTER TABLE `deals`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `dir_marks`
--
ALTER TABLE `dir_marks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `dir_roles`
--
ALTER TABLE `dir_roles`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблицы `dir_sources`
--
ALTER TABLE `dir_sources`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `dir_stages`
--
ALTER TABLE `dir_stages`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT для таблицы `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `deals`
--
ALTER TABLE `deals`
  ADD CONSTRAINT `FK_deal_customers_id` FOREIGN KEY (`id_customers`) REFERENCES `customers` (`id`),
  ADD CONSTRAINT `FK_deal_dir_stages_id` FOREIGN KEY (`id_stage`) REFERENCES `dir_stages` (`id`),
  ADD CONSTRAINT `FK_deal_users_id` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
